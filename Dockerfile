FROM python:3.8-slim-buster

# requirements.txt 파일을 이미지 내 /mlops-app 디렉토리로 복사합니다.
COPY ./requirements.txt /mlops-app/requirements.txt

# requirements.txt 파일에 명시된 Python 라이브러리들을 설치합니다.
RUN pip install --no-cache-dir --upgrade -r /mlops-app/requirements.txt

# mlops-library 디렉토리 전체를 이미지 내 /mlops-app 디렉토리로 복사합니다.
COPY . /mlops-app

# 작업 디렉토리를 /mlops-app으로 설정합니다.
WORKDIR /mlops-app/mlops-library

# 컨테이너가 시작될 때 실행될 명령어를 설정합니다.
CMD ["uvicorn", "main:app", "--host", "0.0.0.0", "--port", "80"]