from setuptools import setup 

setup(
    name="mlops-lib",
    version="0.0.1",
    description="custom library for mlops",
    url="https://gitlab.com/UICHEOL-HWANG/mlops-app",
    author="cheorish",
    packages=["mlops-library"],
    install_requires=[
        "google-cloud-storage==2.6.0"
    ]
    )