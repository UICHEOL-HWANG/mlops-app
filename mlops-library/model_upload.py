from mlops_gcp_client import MLOpsGCSClient

GCP_KEY_FILE = {
  "type": "service_account",
  "project_id": "ascendant-nova-412902",
  "private_key_id": "d35a8d73069dcce7076e2242432b1914d4a90ec6",
  "private_key": "-----BEGIN PRIVATE KEY-----\nMIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQDfpFemgdqoTfa8\nZIiS9I2OD3dBCeX/iINbxEC/e62r5UkIxF7Hz2ti5Kj55Bg/u8z2Qa+JbWG6IHZ8\no7eoQkBpDmQ0JzIkqH1AukpW7Ucr7jyF7eARp+SND9WwKcxoK/RW/TIhwyjZhJVE\nbnfhKU7x3YtOs2cj4xINisjNhGRBUG4laLi3TRseRZPzAgvB1P36SOMo0AvzkZdZ\n8S3zFaNTphirf5b0hZLD+LsIB+rhTFbRSl3OsCTKS7ki9MpB7c7bupiNChhTg+mf\n6K0wHmHeRG6MFavXVIMzr6KDqx0a2tBHJ5uWpWcLUHeiNeymlGXNbMyGfedzS45S\nZIJqWozFAgMBAAECggEAIE3SVVxbc8UzNzpEWgnWKqeEP9JItTUIWGiMO7yHNRW9\nC3WmBTGqXtINsG7oq8gaTpXlX7H7lFGdG9rgK/9J44LvKXNh9FacJQ3jsBO9HJTg\njC30mEhKpK+dW8Fl48Q5mpkfNS5GB8l1cDmOumjnnNs2BKu4YcAqeudJoxoVCzPo\nXJSJcl+rtc0g/dYelBMXbahLAOTksfMsqINiGsJjOYmJzHoLnEIvRQyW/a1xN62u\n/auyCEq/UwC0skeMZMO0I6J0bL8aq4goZ5o2AsoOuhugRYHklHz0i7cwcwiSR+Qp\nKI/9AjYTV63OPCAgPQHCqv8L1DiA1vwoa5bt+LZe/wKBgQDxGhoNANbzOqMmHATI\neY+WBbBEFmtqTlReLSTMToZ9T5S4OpJSp2rxd7GLifnQkp1mkPgSYxEVimnEv8xf\ntf8Y3QvPWZPKh9t1b9hROsftZn2EYPXVwrI7ghBYqaMjZZVpSss16/N7MnAzdCPX\nfWj0qRpjARz09Sumjj+8u7RnewKBgQDtdgw2Z181Men9Xrovhbibp4gsz650po3j\njXmZtkkwe8PxvcXgcAx8Z8VTQ0Cii50BK6D01+t+Ialcwld3QwJVQP0uY7SgSSQU\n13EWhrDG+ISVCMoG/ZexIPpSyIU6lajkIi0caoiqiDeTfBDNQ54VuwMBhV6ln8CY\nhjhIKNiIvwKBgQDJ/P0gMbjjMxfl4IldTwItRHzEvielexQRhi6S/6KKrzZAFcnq\ndxoQTB+xcJma1IoLmJ5MATUZ38j2rVzSPo4FzYKMZ1Z60dDZFSeb/evrBvdXBUhP\nmnppgnChZxUmnX9MI/4VIaTtbHc1lDJG4Wkht95EXXVd/1xfgEMDohnkqwKBgEeY\nwDXnFR3E3vg2B40slGzaJoTcWY1yanWhwnhGLesgZgQNZAw3vIwF/ckNDf/0eHJR\n5UXtlGODYr1FG3oqChoKbkv+ZAewBeEC1GYmVkanx9HnxII3E/WkK9WqAAGZ/kJK\ny681r987Ewa08ZvyN0f2CwH6ZH8rm7ZhQbcAGcmhAoGAFpLVlZMiC5Jel1UUq7Nz\nWvT1WJKFcW/2moub6yd1v5+WUSEOLEbgYszTQDscLhXhca/YeFwGutsQLtiVkOwY\nlDsHnw6dqQujX40SJ3u6Fdvk3KkymiQXv+zP+a4MKetzClGCaT333PKc3PKboM/N\nbho6t/cmnJydH4Re9fAc8u0=\n-----END PRIVATE KEY-----\n",
  "client_email": "882251589964-compute@developer.gserviceaccount.com",
  "client_id": "110019676099579701227",
  "auth_uri": "https://accounts.google.com/o/oauth2/auth",
  "token_uri": "https://oauth2.googleapis.com/token",
  "auth_provider_x509_cert_url": "https://www.googleapis.com/oauth2/v1/certs",
  "client_x509_cert_url": "https://www.googleapis.com/robot/v1/metadata/x509/882251589964-compute%40developer.gserviceaccount.com",
  "universe_domain": "googleapis.com"
}

client = MLOpsGCSClient(GCP_KEY_FILE)

client.upload_model(
    bucket_name="mlops-models-storage",
    model_name="nlp-model",
    local_dir_path="../model"

)